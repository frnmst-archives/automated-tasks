#!/usr/bin/env python3
#
# push_files.py
#
# Copyright (C) 2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
r"""Push files to remote repositories and send notifications."""

import hashlib
import pathlib
import shlex
import shutil
import subprocess
import sys

import fpyutils
import yaml


class SourceDirectoryDoesNotExists(Exception):
    pass


class RepositoryDoesNotExists(Exception):
    pass


class SourceDirectoryAndRepositoryCoincide(Exception):
    pass


class NoRemotesConfigured(Exception):
    pass


def send_notification(message: str, notify: dict):
    if notify['gotify']['enabled']:
        m = notify['gotify']['message'] + '\n' + message
        fpyutils.notify.send_gotify_message(
            notify['gotify']['url'],
            notify['gotify']['token'], m,
            notify['gotify']['title'],
            notify['gotify']['priority'])
    if notify['email']['enabled']:
        fpyutils.notify.send_email(message,
                                   notify['email']['smtp server'],
                                   notify['email']['port'],
                                   notify['email']['sender'],
                                   notify['email']['user'],
                                   notify['email']['password'],
                                   notify['email']['receiver'],
                                   notify['email']['subject'])


if __name__ == '__main__':
    def main():
        configuration_file = shlex.quote(sys.argv[1])
        config = yaml.load(open(configuration_file, 'r'), Loader=yaml.SafeLoader)

        src = pathlib.Path(shlex.quote(config['files']['source_directory']))
        repository = pathlib.Path(shlex.quote(config['files']['repository']))

        if not src.is_dir():
            raise SourceDirectoryDoesNotExists
        if not repository.is_dir():
            raise RepositoryDoesNotExists
        if src == repository:
            raise SourceDirectoryAndRepositoryCoincide

        all_files = list(src.glob(config['files']['pattern']))
        files = list()
        files_changed = 0
        for f in all_files:
            src_file = str(pathlib.Path(src, f.name))
            dst_file = str(pathlib.Path(repository, f.name))

            if pathlib.Path(dst_file).is_file():
                # File exists: use checksum to check if
                # there have been changes.
                m = hashlib.sha512()
                m.update(pathlib.Path(src_file).read_bytes())
                src_checksum = m.hexdigest()
                m = hashlib.sha512()
                m.update(pathlib.Path(dst_file).read_bytes())
                dst_checksum = m.hexdigest()

                if src_checksum != dst_checksum:
                    # File changed.
                    shutil.copyfile(src_file, dst_file)
                    files_changed += 1
                    files.append(pathlib.Path(dst_file).name)
            else:
                # File does not exist.
                shutil.copyfile(src_file, dst_file)
                files_changed += 1
                files.append(pathlib.Path(dst_file).name)

        # Track all files.
        fpyutils.shell.execute_command_live_output(
            config['vcs']['executable']
            + ' '
            + ' '.join(config['vcs']['options']['directory'])
            + ' '
            + str(repository)
            + ' '
            + ' '.join(config['vcs']['commands']['add_all'])
        )

        if files_changed > 0:
            fpyutils.shell.execute_command_live_output(
                config['vcs']['executable']
                + ' '
                + ' '.join(config['vcs']['options']['directory'])
                + ' '
                + str(repository)
                + ' '
                + ' '.join(config['vcs']['commands']['commit'])
                + ' "' + str(files_changed) + ' files added."'
            )
            cmd = (
                [
                    config['vcs']['executable'],
                ]
                + config['vcs']['options']['directory']
                + [
                    str(repository),
                ]
                + config['vcs']['commands']['list_remotes']
            )
            s = subprocess.run(cmd, capture_output=True)
            remotes = s.stdout.decode('UTF-8').split()
            if len(remotes) == 0:
                raise NoRemotesConfigured

            # Get current branch.
            cmd = (
                [
                    config['vcs']['executable'],
                ]
                + config['vcs']['options']['directory']
                + [
                    str(repository),
                ]
                + config['vcs']['commands']['get_branch']
            )
            s = subprocess.run(cmd, capture_output=True)
            branch = s.stdout.decode('UTF-8')

            # Push commit to all remotes.
            for r in remotes:
                fpyutils.shell.execute_command_live_output(
                    config['vcs']['executable']
                    + ' '
                    + ' '.join(config['vcs']['options']['directory'])
                    + ' '
                    + ' '
                    + str(repository)
                    + ' '
                    + ' '.join(config['vcs']['commands']['push'])
                    + ' '
                    + r
                    + ' '
                    + branch
                )

        message = 'push_files.py ' + str(repository) + ': changed ' + str(files_changed) + ' files'
        send_notification(message, config['notify'])

    main()
