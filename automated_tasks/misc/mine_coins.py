#!/usr/bin/env python3
#
# mine_coins.py
#
# Copyright (C) 2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import shlex
import sys

import fpyutils
import yaml

if __name__ == '__main__':
    configuration_file = shlex.quote(sys.argv[1])
    config = yaml.load(open(configuration_file, 'r'), Loader=yaml.SafeLoader)
    profile = shlex.quote(sys.argv[2])

    message = 'mine ' + profile + ' coins started'
    if config['notify']['gotify']['enabled']:
        m = config['notify']['gotify']['message'] + '\n' + message
        fpyutils.notify.send_gotify_message(
            config['notify']['gotify']['url'],
            config['notify']['gotify']['token'], m,
            config['notify']['gotify']['title'],
            config['notify']['gotify']['priority'])
    if config['notify']['email']['enabled']:
        fpyutils.notify.send_email(message,
                                   config['notify']['email']['smtp server'],
                                   config['notify']['email']['port'],
                                   config['notify']['email']['sender'],
                                   config['notify']['email']['user'],
                                   config['notify']['email']['password'],
                                   config['notify']['email']['receiver'],
                                   config['notify']['email']['subject'])
    command = (config['miner']['executable']
               + ' --algo=' + config['profiles'][profile]['cpu']['algorithm']
               + ' --url=' + config['profiles'][profile]['pool']['url']
               + ' --pass=' + config['profiles'][profile]['wallet']['password string']
               + ' --user=' + config['profiles'][profile]['wallet']['user string']
               + ' --threads=' + str(config['profiles'][profile]['cpu']['threads'])
               + ' --cpu-priority ' + str(config['profiles'][profile]['cpu']['priority']))
    fpyutils.shell.execute_command_live_output(command)
